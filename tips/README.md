# 如何做研究？

![How_to_do_Research.png](../images/How_to_do_Research.png)

研究生阶段的学习、研究是人生中重要的一个提升能力的阶段，在这段时间通过开展一个研究课题学会如何去学习知识、如何去思考问题、如何分解问题、如何解决问题、如何评估问题、如何总结工作等等，通过综合练习达到批判性思维、系统性思维等多种思维、工作方法的培养，从而为后续的工作奠定能力基础。希望大家能够认真思考、主动去做，真真切切在这段宝贵的时间达到自己的目标。

由于研究生期间的时间比较短，而所需要学习的东西非常多，因此合理的时间分配是十分重要的。此外对所做的研究需要找到相关资料、了解已有哪些方法、问题有哪些等。因此需要在开始研究之后，制定自己的[《研究计划》](https://gitee.com/pi-lab/Wiki/blob/master/tips/master_life/Guidelines_for_ResearchPlanning.md)。制定计划的时候，不用贪多求全，可以先把已经知道的写好，通过研究过程认识的加深不断完善，从而最终完成研究计划和研究本身。

下面是做研究的方法、心得，最好能读三遍，并深入思考，考虑如何将下面所列的方法论变成自己的习惯。



## 1. 研究、学习的心法、心态

研究不仅仅是时间的付出，更多的是学习、琢磨研究的方法等，因此需要先学习一下学习、研究的心法。在后续的研究过程中，不断加深对方法论的理解，从而提高灵活运用能力。

* [学习成功之道（❤必读）](Tao_of_Success) 
* [How to do Research](https://gitee.com/pi-lab/Wiki/tree/master/tips/research_method/HowtodoResearch.ppt)
* [研究生如何快乐科研](https://gitee.com/pi-lab/Wiki/tree/master/tips/master_life/研究生如何快乐科研.md) 
* [研究生读研期间需要知晓的20条法则（❤必读）](master_life/研究生读研期间需要知晓的20条法则.md)
* [研究生如何快乐科研（❤必读）](https://gitee.com/pi-lab/Wiki/tree/master/tips/master_life/研究生如何快乐科研.md) 
* [研究的心得](https://gitee.com/pi-lab/Wiki/tree/master/tips/master_life/如何研究.md)
* [言两语道读研、百转千回思在先](https://gitee.com/pi-lab/Wiki/tree/master/tips/master_life/言两语道读研.md)
* [复旦大学教授：研生读得好，人生就像开了挂](https://gitee.com/pi-lab/Wiki/tree/master/tips/master_life/研生读得好/复旦大学教授：研生读得好，人生就像开了挂.md)
* [哈佛大学教授：教育的目的不是学会一堆知识，而是学会一种思维](http://mp.weixin.qq.com/s/iVVY1n19bDyMCI7QAXKFlQ)
* [美国首席大法官约翰•罗伯茨毕业典礼致词 - 反鸡汤](https://gitee.com/pi-lab/Wiki/tree/master/tips/master_life/美国首席大法官_毕业典礼致词.md)


## 2. 突破口
想要比较好的研究成果，很好的编程、数学、思维能力是必不可少的。但刚开始的时候各项能力都比较弱，会处处受挫，所以不能着急，每个人需要根据自己的特点，找到一个**突破口**，慢慢从这个突破口取得成绩并获得成就感。
* 例如你觉得理论能力相对好，可以先从理论创新开始，然后补其他基础的短板；
* 如果自己觉得自己的编程能力比较强，可以先从修改程序入手，逐步加入新的想法，然后逐步学习理论知识。



## 3. 文献阅读

读其他人的论文是研究过程的重要一步，只有比较全面的查阅了领域的论文，才能给自己的研究一个清晰的定位。但是研究的论文非常多，如何快速有效地把研究领域的重要、关键论文都覆盖到需要在阅读过程不断思考、总结。

* [如何整理、收集研究资料（❤必读）](https://gitee.com/pi-lab/Wiki/blob/master/tips/research_method/CollectMaterials.md)
* [读科研论文的三个层次、四个阶段与十个问题（❤必读）](https://www.toutiao.com/i6852569034870358541/)
* [Andrew Ng(吴恩达)关于机器学习职业生涯以及阅读论文的一些建议](https://m.toutiaocdn.com/group/6758772461010960910)
* [施一公：低年级研究生应该如何读英文文献？](https://www.toutiao.com/a6825465232589062663/)
* [吴恩达教你如何读论文，高效了解新领域](https://www.toutiao.com/i6845470603835605507/)





## 4. 研究计划

由于研究生期间的时间比较短，而所需要学习的东西非常多，因此合理的时间分配是十分重要的。此外对所做的研究需要找到相关资料、了解已有哪些方法、问题有哪些等。
* 需要在开始研究之后，制定自己的[《研究计划》](https://gitee.com/pi-lab/Wiki/tree/master/tips/master_life/Guidelines_for_ResearchPlanning.md)。
* 制定计划的时候，不要贪多求全，可以把已经知道的写好，通过不断完善的方式最终完成研究计划。
* 研究计划的制定，可参考[《差异检测研究》](https://gitee.com/pi-lab/research_change_detection)中[《研究计划编写例子》](https://gitee.com/pi-lab/research_change_detection/tree/master/liqing)



可以参考[《差异检测研究》](https://gitee.com/pi-lab/research_change_detection)，编写自己的研究方案、计划等。需要完成的内容包括：

* 研究目标，主要思路，关键技术，参考代码，参考文献
* 《研究计划》
* 研究背景、意义，相关研究方法
* 研究方法描述
* 实验结果




## 5. 编程、实现、实验
制定完研究计划并找到突破口，接下来就开始脚踏实地一步一步通过编程实现想法，并完成实验。编程能力对应研究、试错等都是非常重要的技能，因此大家需要高度重视，避免眼高手低的现象。

在实现过程，如果遇到难题无法解决，可以按照下面的思路来思考：
1. 这个难题是否有可以借鉴的程序、论文？
	- 如果有，去哪里找？找到了之后，如何测试、验证？如何整合到自己的程序？
	- 如果没有，评估自己是否有足够的功力去自己解决？是否可以可以和老师、同学一起讨论？
2. 研究的整体能够分成几个大的部分？
	- 分解成小的问题，这样每次专注去解决一个问题
	- 每个小问题的边界、输入输出是什么？
	- 如何单独测试小的问题？

更详细的介绍可以参考[《实现、编程方法论》](https://gitee.com/pi-lab/Wiki/tree/master/tips/research_method/README.md)




## 6. 撰写论文

论文是研究工作的总结，不仅作为自己工作的一个记录，同时也给他人一个学习的资料，通过看别人的论文，写自己的论文，从而完成学术环境的构建。第一次写论文比较痛苦，因此需要用比较好的方法来降低痛苦指数，从而避免中途放弃。主要采用的策略是：`抛砖引玉，迭代完善`。
* [《一步一步学写作》](https://gitee.com/pi-lab/Wiki/tree/master/tips/learn_writting)
* [《论文写作指南》](https://gitee.com/pi-lab/Wiki/tree/master/tips/paper_writting/README.md)
* [《语法检查指南》](https://gitee.com/pi-lab/Wiki/tree/master/tips/paper_writting/GrammarCheck.md)
* [《如何回复审稿意见》](https://gitee.com/pi-lab/Wiki/tree/master/tips/paper_writting/paper_response/README.md)
* [《如何审稿》](https://gitee.com/pi-lab/Wiki/tree/master/tips/paper_writting/paper_review/README.md)
* [33篇顶会论文如何做到？计算机视觉领域，不可不知的关键环节](https://www.toutiao.com/i6835584065580565000/)
* [How to write a good CVPR submission](https://gitee.com/pi-lab/Wiki/tree/master/tips/research_method/HowToWriteGoodCVPR.pdf)
* [How to Get Your CVPR Paper Rejected?](https://gitee.com/pi-lab/Wiki/tree/master/tips/research_method/How-to-get-your-CVPR-paper-rejected.pdf)
* [和导师一起赶文章死线（Deadline）的十大注意事项](https://gitee.com/pi-lab/Wiki/tree/master/tips/research_method/deadline-chen.pdf)



## 7. 撰写专利

专利作为科研过程重要的产物，不仅保护了发明创造者的权利，而且是一个比较重要的工作总结过程。通过专利撰写，梳理自己对所做工作的理解，从而凝练出更高的研究、发明思路。

* [《如何申请专利》](https://gitee.com/pi-lab/Wiki/tree/master/tips/patent_writting/README.md)
* [《一步一步教你成为专利大牛》](https://gitee.com/pi-lab/Wiki/tree/master/tips/patent_writting/如何申请专利.docx)



## 8. 申请基金

申请基金类似融资，需要通过你的分析，让评审的人员认可你计划要做的事情，认为这个研究有很大的研究价值、是非常有必要去做的。

* [《如何申请基金》](https://gitee.com/pi-lab/Wiki/tree/master/tips/funding_proposal/README.md)



## 9. 方法论

学习常用的思维方法、克服拖延症等。

* [《批判性思维》](https://gitee.com/pi-lab/Wiki/tree/master/tips/working_method/思维方法/批判性思维.pptx)
* [优秀的人都是怎样训练大脑的?](https://gitee.com/pi-lab/Wiki/tree/master/tips/master_life/优秀的人都是怎样训练大脑的.md)
* [如何才能学会“深度思考”？](https://www.jianshu.com/p/edb6e45bd0ba)
* [拖延症患者有救了，五招教你战胜拖延症！](https://www.toutiao.com/a6398449441149337858/)



## 10. 更多资料

* [研究心得与方法汇总](README.md)
* [思维与工作方法汇总](https://gitee.com/pi-lab/Wiki/tree/master/tips/working_method/README.md)
* [上海交通大学生存手册](https://github.com/SurviveSJTU/SurviveSJTUManual)
